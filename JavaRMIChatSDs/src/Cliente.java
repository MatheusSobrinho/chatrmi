import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.*;

import javax.swing.*;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.rmi.RemoteException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Cliente extends JFrame {
	static InterTeste janela = new InterTeste();
	static JLabel msgAtual;
	static JPanel msgAtualPanel;
	static Container c = null;
	static String nome;
	static Thread threadMsgs;
	static Thread threadUsers;
	public static int posicaoPessoasSala;

	public static void main(String args[]) throws UnknownHostException {

		String ip = InetAddress.getLocalHost().getHostAddress();
		System.out.println(ip);

		try {
			final ServidorChat chat = (ServidorChat) Naming.lookup("rmi://"
					+ ip + ":1098/ServidorChat");

			String msg = "";

			nome = JOptionPane.showInputDialog("Digite seu nome:*");

			boolean verificaNome = true;

			while (verificaNome) {
				if (nome.length() > 0 && nome != " ") {
					chat.insereUser(nome);
					break;
				} else {
					nome = JOptionPane
							.showInputDialog("Digite seu nome:(Não é permitido nomes nulos)");
				}
			}

			threadUsers = new Thread(new Runnable() {
				@Override
				public void run() {
					posicaoPessoasSala = 0;
					int size = 0;
					try {
						while (true) {
							JLabel nome;
							ArrayList<String> sala = chat.sala();
							if (size != sala.size()) {
								janela.getPanelSala().removeAll();
								for (int i = 0; i < sala.size(); i++) {
									int y = i;
									y = y * 40;
									JPanel painel = new JPanel();
									nome = new JLabel("  " + sala.get(i) + "");
									painel.setBounds(10, y, 200, 30);
									nome.setSize(180, 30);

									painel.setBorder(BorderFactory
											.createMatteBorder(2, 10, 2, 2,
													new Color(0, 200, 0)));
									painel.add(nome);
									janela.getPanelSala().add(painel);
									janela.getPanelSala().repaint();

									janela.getPanelSala().setPreferredSize(
											new Dimension(0, sala.size() * 40));
									InterTeste.getScrollSala().getViewport()
											.setViewPosition(new Point(50, 0));

									System.out.println(sala.get(i));
								}
								size = sala.size();
							}
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			});

			threadMsgs = new Thread(new Runnable() {
				int cont = 0;
				int var = 0;

				@Override
				public void run() {
					try {
						while (true) {
							int tamanho = chat.tamanhoArray();
							if (tamanho > cont) {
								ArrayList<String> array = chat
										.lerMensagem(cont);
								for (int i = 0; i < array.size(); i++) {
									msgAtualPanel = new JPanel();
									msgAtual = new JLabel(array.get(i));

									msgAtualPanel.setBounds(30, var, msgAtual
											.getText().length() * 13 + 20, 30);

									var = var + 40;
									msgAtualPanel.add(msgAtual);
									msgAtualPanel.setBorder(BorderFactory
											.createMatteBorder(2, 2, 4, 2,
													new Color(0, 200, 204)));
									
									janela.getPanelConversa()
											.add(msgAtualPanel);
									janela.getPanelConversa().repaint();
									janela.getPanelConversa().setPreferredSize(
											new Dimension(700, var + 10));
									InterTeste
											.getScrollConversa()
											.getViewport()
											.setViewPosition(
													new Point(700, var + 10));
								}
								cont = tamanho;
							}
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			});
			// iniciação das threads
			threadUsers.start();
			threadMsgs.start();

			// evento do botão enviar
			janela.getBtnEnviar().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String texto = janela.getTxDigitar().getText();
					try {
						if (texto.length() > 0) {
							chat.enviarMensagem(nome + ": " + texto);
							janela.getTxDigitar().setText("");
							janela.getTxDigitar().setCaretPosition(0);
						}
					} catch (RemoteException e1) {

						e1.printStackTrace();
					}
				}
			});

			janela.getTxDigitar().addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {}

				@Override
				public void keyReleased(KeyEvent e) {}

				@Override
				public void keyPressed(KeyEvent e) {
					String texto = janela.getTxDigitar().getText();
					if (e.getKeyCode() == e.VK_ENTER) {
						try {
							if (texto.length() > 0) {
								chat.enviarMensagem(nome + ": " + texto);
								janela.getTxDigitar().setText("");
								janela.getTxDigitar().setCaretPosition(0);
							}
						} catch (RemoteException e1) {
							e1.printStackTrace();
						}
					}
				}
			});

			InterTeste.getBtnSair().addActionListener(new ActionListener() {
				@SuppressWarnings("deprecation")
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						chat.sairChat(nome);
						threadMsgs.stop();
						threadUsers.stop();
						janela.dispose();
						System.out.println("Você saiu do Chat");
						System.exit(0);
					} catch (RemoteException e1) {
						e1.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}