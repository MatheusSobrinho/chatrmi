import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Servidor {
	public Servidor() {
		try {
			String ip = InetAddress.getLocalHost().getHostAddress();
			System.out.println(ip);
			
			Registry registry = LocateRegistry.createRegistry(1098);
			ServidorChat server = new ServidorChatImpl();
		
			Naming.rebind("rmi://"+ip+":1098/ServidorChat", server);
			System.out.println("Servidor criado!");
		} catch (Exception e) {
			System.out.println("Trouble: " + e);
		}

	}

	public static void main(String args[]) {
		new Servidor();
	}
}
