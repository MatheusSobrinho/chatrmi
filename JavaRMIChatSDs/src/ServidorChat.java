import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ServidorChat extends Remote {
	
	public void enviarMensagem(String mensagem) throws RemoteException;

	public ArrayList<String> lerMensagem(int contMensagens) throws RemoteException;
	
	public int tamanhoArray() throws RemoteException;
	
	public void insereUser(String nome) throws RemoteException;
	
	public ArrayList<String> sala() throws RemoteException;
	
	public void sairChat(String nome) throws RemoteException;
}