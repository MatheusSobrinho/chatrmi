import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.InputStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class InterTeste extends JFrame {
	public static JFrame frame;
	public static JPanel panelSala;
	public static JPanel panelConversa;
	public static JTextArea txDigitar;
	public static JButton btnEnviar;
	public static JButton btnSair;
	public static JScrollPane scroll;
	public static JScrollPane scrollConversa;
	public static JScrollPane scrollSala;
	public static ImageIcon fundo;

	public InterTeste() {

		frame = new JFrame("aCHATado");
		frame.setLayout(null);
		frame.setBounds(150, 0, 1050, 720);
		frame.setResizable(false);

		panelSala = new JPanel();
		panelConversa = new JPanel();
		txDigitar = new JTextArea();
		btnEnviar = new JButton("Enviar");
		btnSair = new JButton("Sair");

		
	
		scroll = new JScrollPane(txDigitar);
		scrollConversa = new JScrollPane(panelConversa);
		scrollSala = new JScrollPane(panelSala);
		
		txDigitar.setEditable(true);
		txDigitar.setLineWrap(true);

		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollConversa.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollSala.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		Font font = new Font("Verdana", Font.BOLD, 23);

		panelConversa.setBackground(new Color(200, 230, 200));
		panelSala.setAutoscrolls(isFocusTraversalPolicySet());
		panelSala.setBackground(new Color(200, 200, 200));
		txDigitar.setBackground(new Color(250, 250, 250));
		
		panelConversa.setLayout(null);
		panelSala.setLayout(null);
		panelSala.setSize(200, 600);
		panelConversa.setSize(800, 600);
		
		scroll.setBounds(250, 600, 700, 100);
		scrollConversa.setBounds(250, 0, 800, 600);
		scrollSala.setBounds(0, 0, 250, 600);
		
		txDigitar.setSize(700, 100);
		txDigitar.setFont(font);
		txDigitar.setBorder(null);

		btnEnviar.setBounds(950, 600, 100, 90);
		btnSair.setBounds(50, 650, 80, 25);

		scrollConversa.setPreferredSize(new Dimension(700, 600));
		panelConversa.setPreferredSize(new Dimension(700, 600));
		scrollSala.setPreferredSize(new Dimension(200, 500));
		panelSala.setPreferredSize(new Dimension(200, 500));
		
		
		JLabel fund = new JLabel();
		fund.setIcon(new ImageIcon("imagemFundo.jpg"));
		fund.setPreferredSize(new Dimension(700, 600));
		fund.setBounds(0, 0, 500, 500);
		scrollConversa.add(fund);
		
		frame.add(scrollConversa);
		frame.add(scrollSala);
		frame.add(scroll);
		frame.add(btnEnviar);
		frame.add(btnSair);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static JScrollPane getScrollSala() {
		return scrollSala;
	}

	public static void setScrollSala(JScrollPane scrollSala) {
		InterTeste.scrollSala = scrollSala;
	}

	public static JFrame getFrame() {
		return frame;
	}

	public static void setFrame(JFrame frame) {
		InterTeste.frame = frame;
	}

	public JPanel getPanelSala() {
		return panelSala;
	}

	public void setPanelSala(JPanel panelSala) {
		this.panelSala = panelSala;
	}

	public JPanel getPanelConversa() {
		return panelConversa;
	}

	public void setPanelConversa(JPanel panelConversa) {
		this.panelConversa = panelConversa;
	}

	public JTextArea getTxDigitar() {
		return txDigitar;
	}

	public void setTxDigitar(JTextArea txDigitar) {
		this.txDigitar = txDigitar;
	}

	public JButton getBtnEnviar() {
		return btnEnviar;
	}

	public void setBtnEnviar(JButton btnEnviar) {
		this.btnEnviar = btnEnviar;
	}

	public static JScrollPane getScrollConversa() {
		return scrollConversa;
	}

	public static void setScrollConversa(JScrollPane scrollConversa) {
		InterTeste.scrollConversa = scrollConversa;
	}

	public static JButton getBtnSair() {
		return btnSair;
	}

	public static void setBtnSair(JButton btnSair) {
		InterTeste.btnSair = btnSair;
	}
}