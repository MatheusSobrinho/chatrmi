import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ServidorChatImpl extends java.rmi.server.UnicastRemoteObject
		implements ServidorChat {
	ArrayList<String> bufferMensagens;
	ArrayList<String> usuarios;
	
	int nMensagens;

	public ServidorChatImpl() throws RemoteException {
		super();
		this.bufferMensagens = new ArrayList<String>();
		this.usuarios = new ArrayList<String>();
	}

	public void enviarMensagem(String mensagem) throws RemoteException {
		bufferMensagens.add(mensagem);
	}

	public ArrayList<String> lerMensagem(int contMensagens) throws RemoteException {
		ArrayList<String> retorno = new ArrayList<String>();
		
		if(contMensagens>=0){
			while(contMensagens<tamanhoArray()){
				retorno.add(bufferMensagens.get(contMensagens));
				contMensagens++;
			}
		}else{
			retorno.add("Sem Mensagens no buffer.");
		}
		return retorno;
	}
	
	public int tamanhoArray() throws RemoteException {
		return bufferMensagens.size();
	}

	public void insereUser(String nome) throws RemoteException{
		usuarios.add(nome);
		System.out.println("Entrou no chat: "+nome);
	}
	
	public ArrayList<String> sala() throws RemoteException{
		return usuarios;
	}
	
	public void sairChat(String nome) throws RemoteException {
		for(int i=0;i<usuarios.size();i++){
			if(usuarios.get(i).equals(nome)){
				usuarios.remove(i);
				//break;
			}
		}
	}
}